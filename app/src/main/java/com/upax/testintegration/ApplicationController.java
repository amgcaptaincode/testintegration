package com.upax.testintegration;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.zeus.zcommon.encrypt.ZCEncriptorKey;

public class ApplicationController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //FirebaseApp.initializeApp(this);
        //MultiDex.install(this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        initEncryptorKey();
    }


    private void initEncryptorKey() {
        final String key = "Z3us3ncryPtEd.K3Y.Z3us3ncryPtEd+";
        ZCEncriptorKey.setListener(() -> key);
    }

}
